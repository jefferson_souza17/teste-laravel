API LARAVEL

php artisan serve

após criar as tableas com o comando migrate:

POST : localhost:8000/ofertas

JSON PARA POST:

{
  "titulo": "NIKE",
  "empresa": "LOJAS NIKE",
  "categorias": [
    1
  ],
  "descricao": "OFERTA DE VERÃO",
  "instrucoes": "Rua padre felipe 116 Esteio",
  "tag": [
    2
  ],
  "ativo": 1
}

GET : localhost:8000/ofertas - retorna todas as ofertas

GET : localhost:8000/ofertas/1 - retornar oferta id 1





ELASTICSEARCH

dir/bin -> ./elasticsearch

GET : http://localhost:9200/api/ofertas/1 - recupera oferta id 1

GET: http://localhost:9200/api/ofertas/_search?pretty=true&q=*:* - todas as ofertas

PUT : http://localhost:9200/api/ofertas/6 - cadastrar oferta

JSON PARA PUT:

{
  "titulo": "NIKE",
  "empresa": "LOJAS NIKE",
  "categorias": [
    1
  ],
  "descricao": "OFERTA DE VERÃO",
  "instrucoes": "Rua padre felipe 116 Esteio",
  "tag": [
    2
  ],
  "ativo": 1
}


