<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB; // para usar a classe DB

class OffersController extends Controller
{
  public function getOfertas()
  {
      //SELECT COM TODAS AS OFERTAS
      $get = DB::table('api.ofertas')->select('*')->get();

      foreach ($get as $dados) {

        //BUSCANDO CATEGORIAS
        $get_categorias = DB::table('api.ofertas as offer')
            ->leftJoin('api.aux_categorias as aux', 'offer.id_oferta', '=', 'aux.id_oferta')
            ->leftJoin('api.categorias as cat', 'aux.id_categoria', '=', 'cat.id_categoria')
            ->where('offer.id_oferta', '=', $dados->id_oferta)
            ->groupBy('cat.descricao')
            ->select('cat.descricao as categorias')
            ->get();

            //CRIANDO ARRAY CATEGORIAS
            $catgorias = '';
            foreach ($get_categorias as $categorias) {
              $catgorias[] = $categorias->categorias;
            }

          //BUSCANDO TAGS
          $get_tags = DB::table('api.ofertas as offer')
              ->leftJoin('api.aux_tags as aux', 'offer.id_oferta', '=', 'aux.id_oferta')
              ->leftJoin('api.tags as tag', 'aux.id_tag', '=', 'tag.id_tag')
              ->where('offer.id_oferta', '=', $dados->id_oferta)
              ->groupBy('tag.descricao')
              ->select('tag.descricao as tags')
              ->get();

              //CRIANDO ARRAY DE TAGS
              $tag = '';
              foreach ($get_tags as $tags) {
                $tag[] = $tags->tags;
              }

        //RETORNO COM OS DADOS DAS OFERTAS
        $retorno[] = array('id_oferta'  => $dados->id_oferta,
                           'id_oferta'  => $dados->id_oferta, 'titulo'     => $dados->titulo,
                           'empresa'    => $dados->empresa,
                           'categorias' => $catgorias,
                           'descrição'  => $dados->descricao,
                           'instruções' => $dados->instrucoes,
                           'tag'        => $tag,
                           'ativo'      => $dados->ativo);
      }

      return response()->json($retorno);
  }





  //GET DE OFERTA EM ESPECIFICO
  public function getOferta($id, Request $request){

    //SELECT COM TODAS AS OFERTAS
    $get = DB::table('api.ofertas')->where('id_oferta', '=', $id)->select('*')->get();
    if(sizeof($get) == 0){
      return response()->json(['erro' => 404], 404);
    }

    foreach ($get as $dados) {

      //BUSCANDO CATEGORIAS
      $get_categorias = DB::table('api.ofertas as offer')
          ->leftJoin('api.aux_categorias as aux', 'offer.id_oferta', '=', 'aux.id_oferta')
          ->leftJoin('api.categorias as cat', 'aux.id_categoria', '=', 'cat.id_categoria')
          ->where('offer.id_oferta', '=', $id)
          ->groupBy('cat.descricao')
          ->select('cat.descricao as categorias')
          ->get();

          //CRIANDO ARRAY CATEGORIAS
          $catgorias = '';
          foreach ($get_categorias as $categorias) {
            $catgorias[] = $categorias->categorias;
          }

        //BUSCANDO TAGS
        $get_tags = DB::table('api.ofertas as offer')
            ->leftJoin('api.aux_tags as aux', 'offer.id_oferta', '=', 'aux.id_oferta')
            ->leftJoin('api.tags as tag', 'aux.id_tag', '=', 'tag.id_tag')
            ->where('offer.id_oferta', '=', $id)
            ->groupBy('tag.descricao')
            ->select('tag.descricao as tags')
            ->get();

            //CRIANDO ARRAY DE TAGS
            $tag = '';
            foreach ($get_tags as $tags) {
              $tag[] = $tags->tags;
            }

      //RETORNO COM OS DADOS DAS OFERTAS
      $retorno = array('id_oferta'  => $dados->id_oferta,
                       'titulo'     => $dados->titulo,
                       'empresa'    => $dados->empresa,
                       'categorias' => $catgorias,
                       'descrição'  => $dados->descricao,
                       'instruções' => $dados->instrucoes,
                       'tag'        => $tag,
                       'ativo'      => $dados->ativo);
    }

    return response()->json($retorno);
  }



  //POST DE OFERTA
  public function postOferta(Request $request){
    $data = sizeof($_POST) > 0 ? $_POST : json_decode($request->getContent(), true); // Pega o post ou o raw

    //validar post
    switch ($data) {
      case $data['titulo'] == null:
        return response()->json(['erro titulo' => 400], 400);
        break;
      case $data['empresa'] == null:
        return response()->json(['erro empresa' => 400], 400);
        break;
      case sizeof($data['categorias']) == 0:
        return response()->json(['erro categorias' => 400], 400);
        break;
      case $data['descricao'] == null:
        return response()->json(['erro descricao' => 400], 400);
        break;
      case $data['instrucoes'] == null:
        return response()->json(['erro instrucoes' => 400], 400);
        break;
      case sizeof($data['tag']) == 0:
        return response()->json(['erro tag' => 400], 400);
        break;
      case $data['ativo'] != true && $data['ativo'] != false:
        return response()->json(['erro ativo' => 400], 400);
        break;
    }

	  //INSERINDO REGISTRO NA TABELA OFERTAS E RECUPERANDO ID
    $id_oferta = DB::table('api.ofertas')->insertGetId(
    ['titulo'       => $data['titulo'],
     'empresa'      => $data['empresa'],
     'descricao'    => $data['descricao'],
     'instrucoes'   => $data['instrucoes'],
     'ativo'        => $data['ativo']]
    );

    if(sizeof($id_oferta) == null){
      return response()->json(['erro' => 500], 500);
    }

    $size_tag = sizeof($data['tag']);

    for ($i=0; $i < $size_tag; $i++) {
      //INSERINDO REGISTRO NA TABELA AUXILIAR TAGS
      DB::table('api.aux_tags')->insert([
        ['id_oferta'       => $id_oferta,
         'id_tag'          => $data['tag'][$i]]
      ]);
    }


    $size_cat = sizeof($data['categorias']);

    for ($x=0; $x < $size_cat; $x++) {
      //INSERINDO REGISTRO NA TABELA AUXILIAR TAGS
      DB::table('api.aux_categorias')->insert([
        ['id_oferta'       => $id_oferta,
         'id_categoria'    => $data['categorias'][$x]]
      ]);
    }



		return response()->json(['criou' => 200], 200);
  }




}
