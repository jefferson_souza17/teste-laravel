<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
//CRIANDO ROTAS E UTILIZANDO API COM PREFIXO 'inicio'
Route::group(array('prefix' => 'api'), function()
{

  Route::get('/ofertas', 'OffersController@getOfertas');
  Route::get('/ofertas/{id}', 'OffersController@getOferta');
  Route::post('/ofertas', 'OffersController@postOferta');

  /*Route::get('/oferta/{id}', function ($id) {
      return response()->json(['message' => 'Oferta id = '+$id]);
  })->where('id', '[0-9]+');

  Route::post('/ofertas', function () {
      return response()->json(['nome' => 'post ofertas']);
  });
  */


  Route::resource('offers', 'OffersController');
});

Route::get('/', function () {
    return redirect('api');
});
