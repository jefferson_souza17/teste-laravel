<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //INSERINDO REGISTROS DE TESTES NO BANCO
      DB::table('ofertas')->insert([
        'titulo'      => 'primeiro registro de oferta',
        'empresa'     => 'teste',
        'descricao'   => 'Teste de oferta 99,99',
        'instrucoes'  => 'Para adquirir basta acessar o site ...',
        'ativo'       => TRUE,
        ]);

      DB::table('categorias')->insert([
        'descricao' => 'Roupas e Calçados',
        ]);

      DB::table('tags')->insert([
        'descricao' => 'agasalho',
        ]);

      DB::table('aux_tags')->insert([
        'id_oferta'     => 1,
        'id_tag'        => 1,
        ]);

        DB::table('aux_categorias')->insert([
          'id_oferta'     => 1,
          'id_categoria'  => 1,
          ]);

    }
}
