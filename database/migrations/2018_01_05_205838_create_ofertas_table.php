<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas', function (Blueprint $table) {
          $table->increments('id_oferta');
          $table->string('titulo', 140);
          $table->string('empresa', 70);
          $table->longText('descricao');
          $table->longText('instrucoes');
          $table->boolean('ativo');
          $table->timestamps();//eloquente cria create_at e update_at para controle
          $table->softDeletes();//eloquente cria deleted_at
          //softDeletes() -> eloquente não remove os dados apenas não retorna quando excluídos.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertas');
    }
}
