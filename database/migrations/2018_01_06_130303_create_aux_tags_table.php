<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuxTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aux_tags', function (Blueprint $table) {
          $table->increments('id_auxiliar_tag');
          $table->integer('id_oferta')->unsigned();
          $table->foreign('id_oferta')->references('id_oferta')->on('ofertas')->onDelete('cascade');
          $table->integer('id_tag')->unsigned();
          $table->foreign('id_tag')->references('id_tag')->on('tags')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aux_tags');
    }
}
