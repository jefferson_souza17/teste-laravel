<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuxCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aux_categorias', function (Blueprint $table) {
          $table->increments('id_auxiliar_categorias');
          $table->integer('id_oferta')->unsigned();
          $table->foreign('id_oferta')->references('id_oferta')->on('ofertas')->onDelete('cascade');
          $table->integer('id_categoria')->unsigned();
          $table->foreign('id_categoria')->references('id_categoria')->on('categorias')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aux_categorias');
    }
}
